import React, {Component} from 'react';

class RestaurantDetails extends Component{
  state={
    url:"http://localhost:3000/api/restaurant/",
    details:[],
    id:this.props
  }


  
  async getRestaurantsDetails() {
    let id =1;
    try {
      const data = await fetch(this.state.url+id);
      const jsonData = await data.json();

      if (jsonData.length === 0) {
        this.setState(() => {
          return {error: 'sorry, but your search did not return any results'};
        });
      } else {
        this.setState({
          details: jsonData,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  componentDidMount(){
    this.getRestaurantsDetails()
  }


  render() {
    
  
    return(
      <div >
     <h2 class="bg-primary px-3">
            {this.state.details.name}
          </h2>       </div>   
    ) ;
  }

  // showDetail(){
  //   <Link>
  //   </Link>
  // }
 }

export default RestaurantDetails;