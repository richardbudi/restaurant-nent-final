import React, {Component} from 'react';
import './Nav.css';

const Nav = () => {
  return (
    <nav id="navbar">
      <h1 class="logo">
        <span class="text-primary">
          <i class="fas fa-utensils"></i> The</span> Restaurants
      </h1>
      <ul>
        <li><a href="#restaurants">List</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </nav>
  );
};

export default Nav;