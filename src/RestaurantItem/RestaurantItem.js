import React, {Component} from 'react';
import './RestaurantItem.css';
import {Link} from 'react-router-dom';
import * as ROUTES from '../constants/routes';

class RestaurantItem extends Component {
   
  render() {
    const resto = this.props.resto;
    const {showDetail} = this.props;

    let dollars = `<i class="fas fa-dollar-sign"></i>`.repeat(resto.price_level);
    console.log(resto)
    return(
      <div >
        <div>
          <h2 class="bg-primary px-3">
            {resto.name}
         </h2>      
        </div>
        <div class="description p-1 bg-light">
            <div class="descrestoription-img px-1">              
              <img src={resto.photo} alt="food"/>
            </div>
            <div class="description-detail">
              <h4>Price: {dollars}</h4>
              <h4>Rating: {resto.rating}</h4>            
              <Link to={ROUTES.DETAILS} id={resto.id} class="show-more btn btn-light">Show More</Link>
            </div>
        </div>
      </div>   
    ) ;
  }
}

export default RestaurantItem;